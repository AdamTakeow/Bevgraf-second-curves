#include <GLFW/glfw3.h>
#include <iostream>
#include <vector>
#include <cmath>
#include <unistd.h>

#define PI 3.14159265359

// ############### POINT CLASS ################

class Point
{
public:
  double x,y,z; // temporary

  // constructors
  Point(): x{0.0}, y{0.0}, z{0.0} {} // temporary
  Point(double x, double y): x{x}, y{y}, z{1.0} {} // temporary

  void drawPoint()
  {
    glColor3f(1.0f, 0.0f, 0.0f);
    glPointSize(10.0);

    glBegin(GL_POINTS);
      glVertex2f( x, y );
    glEnd();
  }
};

// ############# MATRIX CLASS ##############

class Matrix
{
private:
  int rowNum; // number of rows in a matrix
	int colNum; // number of cols in a matrix
	double** data; // the actual double elements of the martx

public:
	~Matrix()
	{
		for (int i = 0; i < rowNum; ++i)
		{
			delete[] data[i];
		}
		delete[] data;
	}

// ############## MATRIX CONTRUCTORS ################

	Matrix( int rows, int cols )
	{
		rowNum = rows;
		colNum = cols;
		data = new double*[rowNum];

		for (int i = 0; i < rowNum; ++i)
		{
			data[i] = new double[colNum];
		}

		zeroTheElements();

		// creating an identity matrix
		for (int i = 0; i < rows; ++i)
		{
			data[i][i] = 1.0;
		}
	}

	// matrix copy constructor
	Matrix( const Matrix &other )
	{
		// copy the size
		rowNum = other.getRowNum();
		colNum = other.getColNum();

		data = new double*[rowNum];

		for (int i = 0; i < rowNum; ++i)
		{
			data[i] = new double[colNum];
		}

		// copy the elements
		for (int i = 0; i < rowNum; ++i)
		{
			for (int j = 0; j < colNum; ++j)
			{
				data[i][j] = other[i][j];
			}
		}
	}

	// ############## MATRIX OPERATORS ################

	// matrix copy assignment
	Matrix& operator=( const Matrix& other )
	{
		// if sizes match
		if( rowNum == other.getRowNum() && colNum == other.getColNum() )
		{
			// copy the elements
			for (int i = 0; i < rowNum; ++i)
			{
				for (int j = 0; j < colNum; ++j)
				{
					data[i][j] = other[i][j];
				}
			}
			return *this;
		}
	}

	// Matrix-Matrix multiplication
	Matrix operator*(const Matrix other)
	{
		// if row-column match is present
		if( colNum == other.getRowNum() )
		{
			// create an appropriate sized matrix
			Matrix temp = Matrix( rowNum, other.getColNum() );
			temp.zeroTheElements();

			for (int i = 0; i < rowNum; ++i)
			{
				for (int j = 0; j < other.getColNum(); ++j)
				{
					for (int k = 0; k < colNum; ++k)
					{
						temp[i][j] = temp[i][j] + data[i][k] * other[k][j];
					}
				}
			}
			return temp;
		}
	}

	// subscript operator
	double* operator[](int i) const { return data[i]; }

	// ############## MATRIX FUNCTIONS ################

	void initMoveMatrix( Point d )
	{
		// 1 0 dx
		// 0 1 dy
    // 0 0 1
		zeroTheElements();
		for( int i = 0; i < rowNum; ++i )
		{
			data[i][i] = 1.0;
		}

		data[0][colNum-1] = d.x;
		data[1][colNum-1] = d.y;
	}

	void initRotateMatrixZ( double alpha )
	{
		// cos(y) -sin(y) 0
		// sin(y) cos(y) 0
		// 0 0 1
		data[0][0] = cos( (PI / 180) * alpha );
		data[1][1] = cos( (PI / 180) * alpha );
		data[0][1] = -1.0 * sin( (PI / 180) * alpha );
		data[1][0] = sin( (PI / 180) * alpha );
	}

	void zeroTheElements()
	{
		for (int i = 0; i < rowNum; ++i)
		{
			for (int j = 0; j < colNum; ++j)
			{
				data[i][j] = 0.0;
			}
		}
	}

	int getRowNum() const { return rowNum; }
	int getColNum() const { return colNum; }

// ############ MATRIX INVERSE AND ITS FUNCTIONS ###############

	// returns the inverse of the matrix
	Matrix invert()
	{
		// adjung�lt algebrai aldetermin�ns
		// 1. aldetermin�nsm�trix
		// 2. el�jeles aldetermin�ns m�trix
		// 3. transzpon�lt
		// 4. A^(-1) = Adj(A) / det(A)

		// if the matrix is quadratic
		if (rowNum == colNum)
		{
			// create a clone of this matrix
			Matrix Clone( *this );

			// create result matrix
			Matrix Result( *this );

			// calculate the determinant with LU decomposition
			double det = Clone.determinant();
			// if the determinant is not 0
			if( fabs(det) > 1e-15 )
			{
				double multiply = 1.0 / det;

				for (int i = 0; i < rowNum; ++i)
				{
					for (int j = 0; j < colNum; ++j)
					{
						double temp = Clone.getMinor(i, j).determinant();
						if ((i + j) % 2 == 0)
							Result[i][j] = multiply * temp;
						else
							Result[i][j] = multiply * temp * -1;
					}
				}
			}

			Result.transpose();
			return Result;
		}
	}

	Matrix getMinor(int idel, int jdel)
	{
		// if the matrix is quadratic
		if (rowNum == colNum)
		{
			// create a smaller matrix for the minor
			Matrix temp = Matrix(rowNum - 1, colNum - 1);

			int k = 0;

			// array for the elements ( smaller )
			double elements[(rowNum-1)*(colNum-1)];

			// go through the bigger matrix
			for (int i = 0; i < rowNum; ++i)
			{
				if( i == idel ) continue;
				for (int j = 0; j < colNum; ++j)
				{
					if( j == jdel ) continue;
					// collect the elements of it
					elements[k++] = data[i][j];
				}
			}

			k = 0;
			// go through the smaller matrix
			for( int i = 0; i < rowNum-1; ++i )
			{
				for( int j = 0; j < colNum-1; ++j )
				{
					// copy the elements to there
					temp[i][j] = elements[k++];
				}
			}
			return temp;
		}
	}

	double determinant()
	{
		// if matrix is quadratic
		if (rowNum == colNum)
		{
			// create a clone of this matrix
			Matrix Clone( *this );

			/*LU decomposition*/

			int step = 0; // the actual step
			int swaps = 0; // number of swaps

			for (int i = 0; i < rowNum; ++i)
			{
				int r = Clone.indexOfColumnMax(i, step);

				if( r == -1 )
				{
					// singular
					return 0.0;
				}

				if (i != r) // if the max element of a column is not part of the diagonal
				{
					// swap the diagonal element to the max abs element of that column
					Clone.swapRows(i, r);
					swaps++;
				}
				for (int j = i; j < rowNum - 1; j++)
				{
					// divide every element under the diagonal element with the diagonal element
					Clone[j+1][step] = Clone[j+1][step] / Clone[step][step];
				}
				for (int j = step+1 ; j < rowNum; j++)
				{
					// calculate the inner elements
					// (previos element - under the main element * next to the main element)

					for (int k = step+1; k < rowNum; k++)
					{
						Clone[j][k] = Clone[j][k] - (Clone[step][k] * Clone[j][step]);
					}
				}
				step++; // head to the next step
			}

			if( fabs( Clone[rowNum-1][rowNum-1] ) < 1e-15 )
				// singular
				return 0.0;

			/*end LU decomposition*/

			double determinant = 1.0;

			for (int i = 0; i < rowNum; ++i)
			{
				for (int j = 0; j < rowNum; ++j)
				{
					if ( i == j )
					{
						determinant *= Clone[i][j];
					}
				}
			}

			if (swaps % 2 != 0)
				determinant *= -1;
			return determinant;
		}
	}

	int indexOfColumnMax( int whichColumn, int step )
	{
		double tempMax = 0.0;
		int maxIndex = 0;
		for (int i = step; i < rowNum; ++i)
		{
			if ( fabs(data[i][whichColumn]) > fabs(tempMax) )
			{
				tempMax = data[i][whichColumn];
				maxIndex = i;
			}
		}
		if( fabs( tempMax ) < 1e-15 )
			return -1;
		else
			return maxIndex;
	}

	void swapRows(int which, int swapTo)
	{
		double temp = 0.0;
		for (int i = 0; i < colNum; ++i)
		{
			temp = data[which][i];
			data[which][i] = data[swapTo][i];
			data[swapTo][i] = temp;
		}
	}

	void transpose()
	{
		double temp;
		for( int i = 0; i < rowNum; ++i )
		{
			for( int j = i; j < colNum; ++j )
			{
				temp = data[j][i];
				data[j][i] = data[i][j];
				data[i][j] = temp;
			}
		}
	}
};

// ############### FUNCTION DECLARATIONS ################

void drawBezierCurve( std::vector<Point> );
void drawHermiteCurve( Matrix, Matrix );
void mouseMove(GLFWwindow*, double, double);
void mouseFunc(GLFWwindow*, int, int, int);
void keyPress(GLFWwindow*, int, int, int, int);
int getActivePoint(std::vector<Point>, int, int, int);
void init();
void rotate();
void connectLines();
void calculate_G( Matrix& );
void calculate_M( Matrix&, double[] );

// ############### GLOBAL VARIABLES ################

int windowSize[] = { 640, 480 };
int rotatepoint = -1; // index of the point which is the center of the rotation
bool rotating = false; // rotating or not
bool moving = false; // moving a point or not
int selectedPointIndex = -1; // pointed point index

// contains all the points on the screen
std::vector<Point> points;

int main()
{
  GLFWwindow* window;

  // Initialize the library
  if( !glfwInit() )
  	return -1;

  // Create a windowed mode window and its OpenGL context
  window = glfwCreateWindow( windowSize[0], windowSize[1], "Second curves", NULL, NULL );
  if( !window )
  {
  	glfwTerminate();
  	return -1;
  }

  // Make the window's context current
	glfwMakeContextCurrent(window);

	glfwSetCursorPosCallback(window, mouseMove);
	glfwSetMouseButtonCallback(window, mouseFunc);
	glfwSetKeyCallback(window, keyPress);

	init();

  // first draw the points while putting down
  while( !glfwWindowShouldClose(window) && points.size() < 7 )
	{
    glClear( GL_COLOR_BUFFER_BIT );

    for( auto it: points )
    {
      it.drawPoint();
    }

    glFlush();
		// Swap front and back buffers
		glfwSwapBuffers(window);
		// Poll for and process events
		glfwPollEvents();
  }

  // calculate the touchVector
  Point touchvector = Point(0,0);
  points.push_back( touchvector );
  // the visual touch vector
  // n * ( Pn - Pn-1 )
  points[7].x = 4 * (points[4].x - points[3].x) + points[4].x;
	points[7].y = 4 * (points[4].y - points[3].y) + points[4].y;

  double parameters[] = { 0, 1, 2, 0 };
  Matrix G = Matrix( 2, 4 );
  Matrix M = Matrix( 4, 4 );

  // then until close
  while( !glfwWindowShouldClose(window) )
	{
    glClear( GL_COLOR_BUFFER_BIT );

    // draw the points
    for( auto it: points )
    {
      it.drawPoint();
    }
    // draw the point connections
    connectLines();

    // draw the Bezier curve
    drawBezierCurve( points );

    calculate_G( G );
    calculate_M( M, parameters );
    // draw the Hermite curve
    drawHermiteCurve( G, M );

    // rotate if necessary
    rotate();

    glFlush();
		// Swap front and back buffers
		glfwSwapBuffers(window);
		// Poll for and process events
		glfwPollEvents();
  }

  glfwTerminate();
  return 0;
}

// ############### FUNCTION DEFINITIONS ################

void drawBezierCurve( std::vector<Point> ctlPoints )
{
  // copy the control points
  std::vector<Point> controlPoints = ctlPoints;
  // set color
  glColor3f(0.0f, 1.0f, 0.0f);

  glBegin(GL_LINE_STRIP);

  // de-Casteljau algorithm
  // b^0_i = b_i (i = 0,..,n) control points
  // b^r_i (u) = (1-u) * b^(r-1)_i (u) + u*b^(r-1)_(i+1) (u) (r = 1,...,n) (i = 0,...,n-r)

  // go through the parameters from 0 to 1
  for( GLfloat t = 0; t <= 1; t += 0.01 )
  {
    // biquadratic bezier curve (4)
    for( int r = 0; r < 4; ++r )
    {
      for( int i = 0; i < 4 - r; ++i )
      {
        controlPoints[i].x = (1 - t) * controlPoints[i].x + t * controlPoints[i + 1].x;
        controlPoints[i].y = (1 - t) * controlPoints[i].y + t * controlPoints[i + 1].y;
      }
    }
    glVertex2f( controlPoints[0].x, controlPoints[0].y );
  }
  glEnd();
}

void drawHermiteCurve( Matrix G, Matrix M )
{
  glColor3f(0.5f, 0.0f, 1.0f);

  // GMT formula G*M multiplication
  Matrix C ( G * M );

  glBegin(GL_LINE_STRIP);

  // go through the parameters from 0 to 2
  for (GLfloat t = 0; t <= 2; t += 0.01 )
  {
    // the T "Matrix"
    double T[4] = {t*t*t, t*t, t, 1.0};

    double temp_x = 0.0;
    double temp_y = 0.0;

    // C*T formula
    for( int j = 0; j < 4; ++j )
    {
        temp_x += C[0][j] * T[j];
        temp_y += C[1][j] * T[j];
    }

    glVertex2f( temp_x, temp_y );
  }
	glEnd();
}

void mouseMove( GLFWwindow* window, double xMouse, double yMouse )
{
  // if we are currently moving a point
  if( moving )
  {
    // if we are really pointing to a point not to the white area
    if( selectedPointIndex != -1 )
    {
      // set the point's new position
      points[selectedPointIndex].x = xMouse;
			points[selectedPointIndex].y = yMouse;

      // and if we moved the third point
      if( selectedPointIndex == 3 )
      {
        // recalculate the touchVector
        points[7].x = 4 * (points[4].x - points[3].x) + points[4].x;
      	points[7].y = 4 * (points[4].y - points[3].y) + points[4].y;
      }
      // or if we movec the fourth point
      else if( selectedPointIndex == 4 )
      {
        // also recalculate the touchvector
        points[7].x = points[4].x + (( points[4].x - points[3].x ) * 4);
				points[7].y = points[4].y + (( points[4].y - points[3].y ) * 4);
      }
      // if we mouved the touchvector
      else if( selectedPointIndex == 7 )
      {
        // recalculate the third point
        points[3].x = points[4].x - ( (points[7].x - points[4].x) / 4 );
				points[3].y = points[4].y - ( (points[7].y - points[4].y) / 4 );
      }
    }
    else // if we are on a white area
    {
      // seek for points
      selectedPointIndex = getActivePoint( points, 8, xMouse, yMouse );
    }
  } // if( moving ) end
}

void mouseFunc(GLFWwindow* window, int button, int action, int mods)
{
  // if we press the left mouse button
  if( action == GLFW_PRESS && button == GLFW_MOUSE_BUTTON_LEFT )
  {
    double x,y;
    // get the mouse position
    glfwGetCursorPos(window, &x, &y);

    // create a point from the position
    Point dot = Point( x, y );

    // if we has not got enough point yet
    if( points.size() < 7 )
    {
      points.push_back( dot ); // store the point to the vector
    }
    else // if we has enough point
    {
      moving = true; // then we probably want to move an existing point
    }
  }

  // if we release the left mouse button
  if( action == GLFW_RELEASE && button == GLFW_MOUSE_BUTTON_LEFT )
	{
		moving = false; // we do not want to move anymore
		selectedPointIndex = -1; // we point to nowhere
	}
}

void keyPress(GLFWwindow* window, int pressedKey, int scancode, int action, int mod)
{
  // if we press a key
	if( action == GLFW_PRESS )
	{
		if
    (
      rotating && // if we already rotating
      (
        (pressedKey == GLFW_KEY_1 && rotatepoint == 0) ||
        (pressedKey == GLFW_KEY_2 && rotatepoint == 1) ||
        (pressedKey == GLFW_KEY_3 && rotatepoint == 2) ||
        (pressedKey == GLFW_KEY_4 && rotatepoint == 3) ||
        (pressedKey == GLFW_KEY_5 && rotatepoint == 4) ||
        (pressedKey == GLFW_KEY_6 && rotatepoint == 5) ||
        (pressedKey == GLFW_KEY_7 && rotatepoint == 6) // and we pressed a num key
      )
    )
		{
			rotating = false; // then stop rotating
		}
		else
		{
			switch( pressedKey )
			{
				case GLFW_KEY_1:
          rotating = true;
					rotatepoint = 0;
					break;
				case GLFW_KEY_2:
          rotating = true;
					rotatepoint = 1;
					break;
				case GLFW_KEY_3:
          rotating = true;
					rotatepoint = 2;
					break;
				case GLFW_KEY_4:
          rotating = true;
					rotatepoint = 3;
					break;
				case GLFW_KEY_5:
          rotating = true;
					rotatepoint = 4;
					break;
				case GLFW_KEY_6:
          rotating = true;
					rotatepoint = 5;
					break;
				case GLFW_KEY_7:
          rotating = true;
					rotatepoint = 6;
					break;
			}
		}
	}
}

int getActivePoint( std::vector<Point> points, int sensitivity, int x, int y )
{
  // go through all points
	for( int i = 0; i < points.size(); ++i )
	{
    // if we close enough to a point's coordinates
		if( fabs( points[i].x - x ) < sensitivity && fabs( points[i].y - y ) < sensitivity )
		{
			return i; // return that point
		}
	}
	return -1;
}

void init()
{
  glClearColor (1.0, 1.0, 1.0, 0.0);

  glMatrixMode (GL_PROJECTION);
	glLoadIdentity();
  glOrtho(
  		0.0,                   // left
  		(double)windowSize[0], // right
  		(double)windowSize[1], // bottom
  		0.0,                   // top
  		0.0,
  		1.0
  	);
		glEnable(GL_POINT_SMOOTH);
}

void rotate()
{
  // if we are rotating
  if( rotating )
  {
    for( int i = 0; i < 8; ++i )
    {
      if( i != rotatepoint ) // rotate every point except the rotatepoint
      {

          Matrix moveTo = Matrix( 3,3 );
          // Move the rotatepoint to the origo and the other points move with it too
          moveTo.initMoveMatrix( Point( -1.0 * points[rotatepoint].x, -1.0 * points[rotatepoint].y ) );

          Matrix forgatas = Matrix( 3,3 );
          // rotate around the Z axis
          forgatas.initRotateMatrixZ( 0.05 );

          Matrix moveBack = Matrix( 3,3 );
          // move everything back to its original place
          moveBack.initMoveMatrix( Point(points[rotatepoint].x, points[rotatepoint].y) );


          Matrix Res = moveBack * forgatas * moveTo;

          points[i].x = Res[0][0] * points[i].x + Res[0][1] * points[i].y + Res[0][2] * points[i].z;
      		points[i].y = Res[1][0] * points[i].x + Res[1][1] * points[i].y + Res[1][2] * points[i].z;
      		points[i].z = Res[2][0] * points[i].x + Res[2][1] * points[i].y + Res[2][2] * points[i].z;
      }
    }
  }
}

void connectLines()
{
   // connect the bezier curve's controlpoints
   glColor3f( 0.0f, 0.0f, 1.0f );

  // touchvector
	glLineWidth( 2.0 );

  glEnable(GL_LINE_STIPPLE);
	glLineStipple(1, 0xFF00);

	glBegin( GL_LINES );
			glVertex2f( points[4].x, points[4].y );
			glVertex2f( points[7].x, points[7].y );
	glEnd();

	glDisable(GL_LINE_STIPPLE);

  // other lines
	glColor3f( 0.0f, 0.0f, 0.0f );

	glBegin( GL_LINES );
		glVertex2f( points[0].x, points[0].y );
		glVertex2f( points[1].x, points[1].y );
		glVertex2f( points[1].x, points[1].y );
		glVertex2f( points[2].x, points[2].y );
		glVertex2f( points[2].x, points[2].y );
		glVertex2f( points[3].x, points[3].y );
		glVertex2f( points[3].x, points[3].y );
		glVertex2f( points[4].x, points[4].y );
	glEnd();
}

// find G
void calculate_G( Matrix& G )
{
  int k = 0;
  // from the points vector
  for( int i = 4; i < 7; ++i )
  {
    G[0][k] = points[i].x;
    G[1][k] = points[i].y;
    k++;
  }
  // and the last geometry data, the touchVector
  G[0][k] = 4 * (points[4].x - points[3].x);
  G[1][k] = 4 * (points[4].y - points[3].y);
}

// find M
void calculate_M( Matrix& M, double parameters[] )
{
  for( int i = 0; i < 4; ++i )
    {
      // if we reach the touchVector in the G matrix
      if( i == 3 )
      {
        M[0][i] = 3.0 * parameters[i] * parameters[i]; // 3*t^2
        M[1][i] = 2.0 * parameters[i];                 // 2*t
        M[2][i] = 1.0;                                 // 1
        M[3][i] = 0.0;                                 // 0
      }
      else // we handle the points
      {
        M[0][i] = parameters[i] * parameters[i] * parameters[i]; // t^3
        M[1][i] = parameters[i] * parameters[i];                 // t^2
        M[2][i] = parameters[i];                                 // t
        M[3][i] = 1.0;                                           // 1
      }
    }
    M = M.invert();
}
